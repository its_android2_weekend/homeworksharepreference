package com.example.homeworksharepreference;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;

import com.example.homeworksharepreference.Utils.PreferenceUtil;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;

import java.nio.channels.InterruptedByTimeoutException;

public class AppIntroActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // AppIntro will do the rest.
        SliderPage sliderPage = new SliderPage();
        sliderPage.setTitle("Page 1");
        sliderPage.setDescription("description");
        sliderPage.setImageDrawable(R.mipmap.ic_launcher);
        sliderPage.setBgColor(getResources().getColor(R.color.colorAccent));
        setFadeAnimation();
        addSlide(AppIntroFragment.newInstance(sliderPage));
        addSlide(AppIntroFragment.newInstance(sliderPage));

        //Check required Permission;

    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        StartLogin();
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        StartLogin();
    }
    private void StartLogin(){
        PreferenceUtil.SaveUserStatusPreference(this,2); //Update Status code;
        Intent intent = new Intent(this,LoginActivity.class);//Start Login;
        startActivity(intent);
        finish(); //Close AppIntro;
    }
}
