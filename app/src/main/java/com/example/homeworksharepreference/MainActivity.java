package com.example.homeworksharepreference;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;

import com.example.homeworksharepreference.Utils.PreferenceUtil;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int userStatus = PreferenceUtil.ReadUserStatusCode(this);

        Intent i = null;
        switch (userStatus){
            case 1:
                //ShowAppIntro;
                i = new Intent(this,AppIntroActivity.class);
                startActivity(i);
                finish();
                break;
            case 2:
                //ShowLogin;
                i = new Intent(this,LoginActivity.class);
                startActivity(i);
                finish();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.LogOut:
                PreferenceUtil.SaveUserStatusPreference(this,2);
                startActivity(new Intent(this,LoginActivity.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
