package com.example.homeworksharepreference.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceUtil {
    private static final String PREFERENCE_KEY= "userstatus_pref";
    private static final String USERSTATUS_KEY= "userstatus";

    public static void SaveUserStatusPreference(Context context,int userstatusCode){
        SharedPreferences sharedPreferences = context.getSharedPreferences("",context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(USERSTATUS_KEY,userstatusCode);
        editor.apply();
    }
    public static int ReadUserStatusCode(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("",context.MODE_PRIVATE);
        return sharedPreferences.getInt(USERSTATUS_KEY,1);
    }
}
