package com.example.homeworksharepreference;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.homeworksharepreference.Utils.PreferenceUtil;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnLogin = findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnLogin:
                //If User login Success
                PreferenceUtil.SaveUserStatusPreference(this,3);
                Intent intent = new Intent(this,MainActivity.class);//Start Main;
                startActivity(intent);
                finish(); //Close Login;
                break;
        }
    }
}
